<?php
/**
 * @file
 * business_directory.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function business_directory_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create business content'.
  $permissions['create business content'] = array(
    'name' => 'create business content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any business content'.
  $permissions['delete any business content'] = array(
    'name' => 'delete any business content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own business content'.
  $permissions['delete own business content'] = array(
    'name' => 'delete own business content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any business content'.
  $permissions['edit any business content'] = array(
    'name' => 'edit any business content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own business content'.
  $permissions['edit own business content'] = array(
    'name' => 'edit own business content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
