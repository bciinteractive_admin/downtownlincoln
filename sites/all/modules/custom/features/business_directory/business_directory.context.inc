<?php
/**
 * @file
 * business_directory.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function business_directory_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'business-detail';
  $context->description = 'Layout of Business node detail';
  $context->tag = 'business';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'business' => 'business',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-1' => array(
          'module' => 'block',
          'delta' => '1',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-list_business-block_1' => array(
          'module' => 'views',
          'delta' => 'list_business-block_1',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'authorcontact-0' => array(
          'module' => 'authorcontact',
          'delta' => '0',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Layout of Business node detail');
  t('business');
  $export['business-detail'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'categories';
  $context->description = 'Layout of each category';
  $context->tag = 'pages';
  $context->conditions = array(
    'taxonomy_term' => array(
      'values' => array(
        'business_category' => 'business_category',
      ),
      'options' => array(
        'term_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-categories-block_3' => array(
          'module' => 'views',
          'delta' => 'categories-block_3',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Layout of each category');
  t('pages');
  $export['categories'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'home-default';
  $context->description = 'Layout of Home Default';
  $context->tag = 'home';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'reviews' => 'reviews',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-slideshow-block_1' => array(
          'module' => 'views',
          'delta' => 'slideshow-block_1',
          'region' => 'preface_first',
          'weight' => '-10',
        ),
        'block-16' => array(
          'module' => 'block',
          'delta' => '16',
          'region' => 'preface_second',
          'weight' => '-10',
        ),
        'views-category_restaurant-block_1' => array(
          'module' => 'views',
          'delta' => 'category_restaurant-block_1',
          'region' => 'content_top_second',
          'weight' => '-10',
        ),
        'views-category_automotive-block_1' => array(
          'module' => 'views',
          'delta' => 'category_automotive-block_1',
          'region' => 'content_top_second',
          'weight' => '-9',
        ),
        'views-category_spa-block_1' => array(
          'module' => 'views',
          'delta' => 'category_spa-block_1',
          'region' => 'content_top_second',
          'weight' => '-8',
        ),
        'views-category_nightlife-block_1' => array(
          'module' => 'views',
          'delta' => 'category_nightlife-block_1',
          'region' => 'content_top_second',
          'weight' => '-7',
        ),
        'views-category_shopping-block_1' => array(
          'module' => 'views',
          'delta' => 'category_shopping-block_1',
          'region' => 'content_top_second',
          'weight' => '-6',
        ),
        'views-category_travel-block_1' => array(
          'module' => 'views',
          'delta' => 'category_travel-block_1',
          'region' => 'content_top_second',
          'weight' => '-5',
        ),
        'block-18' => array(
          'module' => 'block',
          'delta' => '18',
          'region' => 'image_background_top',
          'weight' => '-10',
        ),
        'block-19' => array(
          'module' => 'block',
          'delta' => '19',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'block-11' => array(
          'module' => 'block',
          'delta' => '11',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'views-news-block' => array(
          'module' => 'views',
          'delta' => 'news-block',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'views-member-block' => array(
          'module' => 'views',
          'delta' => 'member-block',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Layout of Home Default');
  t('home');
  $export['home-default'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'map-layers';
  $context->description = 'Layout of the Map layer demo page';
  $context->tag = 'pages';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/43' => 'node/43',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'leaflet_demo-leaflet_maps' => array(
          'module' => 'leaflet_demo',
          'delta' => 'leaflet_maps',
          'region' => 'content',
          'weight' => '2',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Layout of the Map layer demo page');
  t('pages');
  $export['map-layers'] = $context;

  return $export;
}
