<?php
/**
 * @file
 * Facebook Likebox bean plugin.
 */

class FBLikebox extends BeanPlugin {
  /**
   * Declares default block settings.
   */
  public function values() {
    $values = array();
    foreach (fb_likebox_block_configure(0) as $fieldset_name => $fieldset) {
      $values[$fieldset_name] = array();
      foreach (element_children($fieldset) as $element) {
        $values[$fieldset_name][$element] = $fieldset[$element]['#default_value'];
      }
    }
    // Overriding
    $values['fb_likebox_display_settings']['fb_likebox_url'] = 'https://www.facebook.com/BciInteractive';
    return $values;
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {
    $form = fb_likebox_block_configure(0);

    foreach ($form as $fieldset_name => &$fieldset) {
      $fieldset['#tree'] = TRUE;
      foreach (element_children($fieldset) as $element) {
        $fieldset[$element]['#default_value'] = $this->default_value($bean, $element, $fieldset_name);
      }
    }
    // Overriding
    $form['fb_likebox_display_settings']['fb_likebox_url']['#description'] = 'Enter the Facebook Page URL. I.e.: https://www.facebook.com/BciInteractive';
    return $form;
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {
    $variables = array();
    foreach (fb_likebox_block_configure(0) as $fieldset_name => $fieldset) {
      foreach (element_children($fieldset) as $element) {
        $var_name = preg_replace('/^fb_likebox_/', 'fb_' , $element);
        $variables[$var_name] = $this->default_value($bean, $element, $fieldset_name);
      }
    }
    $content['facebook_results']['#markup'] = theme('fb_likebox_facebook', $variables);
    return $content;
  }

  /**
   * Returns set setting value, if it is set, or default value otherwise.
   */
  function default_value($bean, $setting, $namespace) {
    if (isset($bean->{$namespace}[$setting])) {
      return $bean->{$namespace}[$setting];
    }
    else {
      $defaults = $this->values();
      return isset($defaults[$namespace][$setting]) ? $defaults[$namespace][$setting] : '';
    }
  }

}
